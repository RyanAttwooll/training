﻿using System.Text.Json.Serialization;
using WeatherApp.CurrentWeatherApp.OpenWeatherMap.Converters;

namespace WeatherApp.CurrentWeatherApp.OpenWeatherMap
{
    public class Error
    {
        [JsonPropertyName("cod")]
        [JsonConverter(typeof(StringConverter))]
        public string Code { get; set; }

        public string Message { get; set; }
    }
}
