﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace WeatherApp.CurrentWeatherApp.OpenWeatherMap
{
    public class List
    {
        [JsonPropertyName("list")]
        public IEnumerable<CurrentWeather> ListWeather { get; set; }

        [JsonPropertyName("city")]
        public CurrentWeather City { get; set; }
    }
}
