﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace WeatherApp.CurrentWeatherApp.OpenWeatherMap
{
    public class Name
    {
        [JsonPropertyName("name")]
        public string CityName { get; set; }
    }
}
