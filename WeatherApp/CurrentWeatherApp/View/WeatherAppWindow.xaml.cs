﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WeatherApp.CurrentWeatherApp.OpenWeatherMap;
using WeatherApp.CurrentWeatherApp;
using WeatherApp.CurrentWeatherApp.View;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.DependencyInjection;

namespace WeatherApp.CurrentWeatherApp
{
    /// <summary>
    /// Interaction logic for WeatherAppWindow.xaml
    /// </summary>
    public partial class WeatherAppWindow : Window
    {
        private readonly IWeatherService weatherService;
        string city = Settings.Default.City;
        public WeatherAppWindow(IWeatherService weatherService)
        {
            InitializeComponent();
            this.weatherService = weatherService;

            var day2 = DateTime.Now.AddDays(1);
            var day3 = DateTime.Now.AddDays(2);
            var day4 = DateTime.Now.AddDays(3);
            Day2.Text = day2.ToString("ddd, dd");
            Day3.Text = day3.ToString("ddd, dd");
            Day4.Text = day4.ToString("ddd, dd");
            Loaded += WeatherAppWindow_Loaded;

        }

        private async void WeatherAppWindow_Loaded(object sender, RoutedEventArgs e)
        {
            var currentWeatherResponse =
                    await weatherService.GetWeatherAsync(city);

            if (currentWeatherResponse.IsSuccessStatusCode)
            {
                var weather = new WeatherModel(currentWeatherResponse.Content);
                ConditionCityTextBlock.Text = weather.Name;
                ConditionImage.Source = new BitmapImage(new Uri(weather.ConditionIconUrl));
                
                TemperatureMinTextBlock.Text = $"Min: {Math.Round(weather.TemperatureMin)} °C";
                TemperatureMaxTextBlock.Text = $"Max: {Math.Round(weather.TemperatureMax)} °C";
                ConditionTextBlock.Text = weather.Condition;

                ConditionImage2.Source = new BitmapImage(new Uri(weather.ConditionIconUrl2));
                TemperatureMinTextBlock2.Text = $"{Math.Round(weather.TemperatureMin2)} °C";
                TemperatureMaxTextBlock2.Text = $"{Math.Round(weather.TemperatureMax2)} °C";
                ConditionTextBlock2.Text = weather.Condition2;

                ConditionImage3.Source = new BitmapImage(new Uri(weather.ConditionIconUrl3));
                TemperatureMinTextBlock3.Text = $"{Math.Round(weather.TemperatureMin3)} °C";
                TemperatureMaxTextBlock3.Text = $"{Math.Round(weather.TemperatureMax3)} °C";
                ConditionTextBlock3.Text = weather.Condition3;

                ConditionImage4.Source = new BitmapImage(new Uri(weather.ConditionIconUrl4));
                TemperatureMinTextBlock4.Text = $"{Math.Round(weather.TemperatureMin4)} °C";
                TemperatureMaxTextBlock4.Text = $"{Math.Round(weather.TemperatureMax4)} °C";
                ConditionTextBlock4.Text = weather.Condition4;

                if (DateTime.Now.Hour < 20)
                {
                    TemperatureTextBlock.Text = $"{Math.Round(weather.Day)} °C";
                }
                else
                {
                    TemperatureTextBlock.Text = $"{Math.Round(weather.Night)} °C";
                }
            }
            else
            {
                MessageBox.Show("Invalid City");
                WeatherSettings WeatherSettings = new WeatherSettings();

                WeatherSettings.Show();
            }

        }

        private void SettingsBtn_Click(object sender, RoutedEventArgs e)
        {
            WeatherSettings WeatherSettings = new WeatherSettings();
            
            WeatherSettings.Show();

        }

        private async void UpdateBtn_Click(object sender, RoutedEventArgs e)
        {
            string city = Settings.Default.City;
            var currentWeatherResponse =
                await weatherService.GetWeatherAsync(city);

            if (currentWeatherResponse.IsSuccessStatusCode)
            {
                var weather = new WeatherModel(currentWeatherResponse.Content);
                ConditionCityTextBlock.Text = weather.Name;
                ConditionImage.Source = new BitmapImage(new Uri(weather.ConditionIconUrl));

                TemperatureMinTextBlock.Text = $"Min: {Math.Round(weather.TemperatureMin)} °C";
                TemperatureMaxTextBlock.Text = $"Max: {Math.Round(weather.TemperatureMax)} °C";
                ConditionTextBlock.Text = weather.Condition;

                ConditionImage2.Source = new BitmapImage(new Uri(weather.ConditionIconUrl2));
                TemperatureMinTextBlock2.Text = $"{Math.Round(weather.TemperatureMin2)} °C";
                TemperatureMaxTextBlock2.Text = $"{Math.Round(weather.TemperatureMax2)} °C";
                ConditionTextBlock2.Text = weather.Condition2;

                ConditionImage3.Source = new BitmapImage(new Uri(weather.ConditionIconUrl3));
                TemperatureMinTextBlock3.Text = $"{Math.Round(weather.TemperatureMin3)} °C";
                TemperatureMaxTextBlock3.Text = $"{Math.Round(weather.TemperatureMax3)} °C";
                ConditionTextBlock3.Text = weather.Condition3;

                ConditionImage4.Source = new BitmapImage(new Uri(weather.ConditionIconUrl4));
                TemperatureMinTextBlock4.Text = $"{Math.Round(weather.TemperatureMin4)} °C";
                TemperatureMaxTextBlock4.Text = $"{Math.Round(weather.TemperatureMax4)} °C";
                ConditionTextBlock4.Text = weather.Condition4;

                if (DateTime.Now.Hour < 20)
                {
                    TemperatureTextBlock.Text = $"{Math.Round(weather.Day)} °C";
                }
                else
                {
                    TemperatureTextBlock.Text = $"{Math.Round(weather.Night)} °C";
                }
            }
        }
    }
}