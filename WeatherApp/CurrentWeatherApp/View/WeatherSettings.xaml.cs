﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WeatherApp;
namespace WeatherApp.CurrentWeatherApp.View
{
    /// <summary>
    /// Interaction logic for WeatherSettings.xaml
    /// </summary>
    public partial class WeatherSettings : Window
    {
        
        public WeatherSettings()
        {
            InitializeComponent();
            Citylbl.Content = ("Current City is: " + Settings.Default.City);
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            if (CityBox.Text == "")
            {
                MessageBox.Show("Enter a city before clicking save");
                
            }
            
            else
            {
                Settings.Default.City = CityBox.Text.ToString();
                Settings.Default.Save();
                Citylbl.Content = ("Current City is: " + Settings.Default.City);
                Instructions.Content = "Now click update on the weather window";
            }
        }
    }
}
