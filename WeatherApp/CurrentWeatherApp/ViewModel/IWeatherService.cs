﻿using Refit;
using System.Threading;
using System.Threading.Tasks;
using WeatherApp.CurrentWeatherApp.OpenWeatherMap;

namespace WeatherApp.CurrentWeatherApp
{
    public interface IWeatherService
    {
        [Get("/daily")]
        Task<ApiResponse<List>> GetWeatherAsync([AliasAs("q")] string city,
            CancellationToken cancellationToken = default);
    }
}