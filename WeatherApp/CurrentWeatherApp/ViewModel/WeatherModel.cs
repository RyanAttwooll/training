﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using WeatherApp.CurrentWeatherApp.OpenWeatherMap;

namespace WeatherApp.CurrentWeatherApp
{
    public class WeatherModel
    {
        public string Name { get; }

        public string ConditionIcon { get; }

        public string ConditionIconUrl => $"https://openweathermap.org/img/w/{ConditionIcon}.png";

        public string Condition { get; }



        public decimal Day { get; }
        public decimal Night { get; }


        public decimal TemperatureMin { get; }

        public decimal TemperatureMax { get; }


        public string Condition2 { get; }
        public string ConditionIcon2 { get; }
        public string ConditionIconUrl2 => $"https://openweathermap.org/img/w/{ConditionIcon2}.png";
        public decimal TemperatureMin2 { get; }
        public decimal TemperatureMax2 { get; }


        public string Condition3 { get; }
        public string ConditionIcon3 { get; }
        public string ConditionIconUrl3 => $"https://openweathermap.org/img/w/{ConditionIcon3}.png";
        public decimal TemperatureMin3 { get; }
        public decimal TemperatureMax3 { get; }

        public string Condition4 { get; }
        public string ConditionIcon4 { get; }
        public string ConditionIconUrl4 => $"https://openweathermap.org/img/w/{ConditionIcon4}.png";
        public decimal TemperatureMin4 { get; }
        public decimal TemperatureMax4 { get; }



        public WeatherModel(List weather)
        {
            
            Condition = weather.ListWeather.First().Conditions.First().Description;
            ConditionIcon = weather.ListWeather.First().Conditions.First().ConditionIcon;
            Day = weather.ListWeather.First().Temp.Day;
            Night = weather.ListWeather.First().Temp.Night;
            TemperatureMax = weather.ListWeather.First().Temp.Max;
            TemperatureMin = weather.ListWeather.First().Temp.Min;
            Name = weather.City.CityName;

            Condition2 = weather.ListWeather.Skip(1).First().Conditions.First().Description;
            ConditionIcon2 = weather.ListWeather.Skip(1).First().Conditions.First().ConditionIcon;
            TemperatureMax2 = weather.ListWeather.Skip(1).First().Temp.Max;
            TemperatureMin2 = weather.ListWeather.Skip(1).First().Temp.Min;


            Condition3 = weather.ListWeather.Skip(2).First().Conditions.First().Description;
            ConditionIcon3 = weather.ListWeather.Skip(2).First().Conditions.First().ConditionIcon;
            TemperatureMax3 = weather.ListWeather.Skip(2).First().Temp.Max;
            TemperatureMin3 = weather.ListWeather.Skip(2).First().Temp.Min;

            Condition4 = weather.ListWeather.Skip(3).First().Conditions.First().Description;
            ConditionIcon4 = weather.ListWeather.Skip(3).First().Conditions.First().ConditionIcon;
            TemperatureMax4 = weather.ListWeather.Skip(3).First().Temp.Max;
            TemperatureMin4 = weather.ListWeather.Skip(3).First().Temp.Min;
        }
    }
}