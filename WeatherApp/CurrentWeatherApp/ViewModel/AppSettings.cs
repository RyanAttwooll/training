﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WeatherApp.CurrentWeatherApp
{
    public class AppSettings
    {
        public string OpenWeatherMapUrl { get; set; }

        public string OpenWeatherMapApiKey { get; set; }
    }
}
