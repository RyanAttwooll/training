﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Text.Json;
using WeatherApp.CurrentWeatherApp;
using WeatherApp.CurrentWeatherApp.OpenWeatherMap.Converters;
using WeatherApp.CurrentWeatherApp.OpenWeatherMap;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Polly;
using Refit;



namespace WeatherApp
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private readonly IHost host;

        private static readonly JsonSerializerOptions jsonSerializerOptions
            = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true,
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase
            };

        public App()
        {
            host = Host.CreateDefaultBuilder()
                .ConfigureServices((context, services) =>
                {
                    ConfigureServices(context, services);
                })
                .Build();
        }

        private void ConfigureServices(HostBuilderContext context, IServiceCollection services)
        {
            var appSettingsSection = context.Configuration.GetSection(nameof(AppSettings));
            var appSettings = appSettingsSection.Get<AppSettings>();
            services.Configure<AppSettings>(appSettingsSection);

            services.AddRefitClient<IWeatherService>(new RefitSettings
            {
                ContentSerializer = new SystemTextJsonContentSerializer(jsonSerializerOptions)
            })
            .ConfigureHttpClient(client =>
            {
                client.BaseAddress = new Uri("http://api.openweathermap.org/data/2.5/forecast");
            })
            .ConfigurePrimaryHttpMessageHandler(_ =>
            {
                var handler = new QueryStringInjectorHttpMessageHandler();
                handler.Parameters.Add("units", "metric");
                handler.Parameters.Add("APPID", "8ebc91462ca4ef611d4fe64c7a674709");
                handler.Parameters.Add("cnt", "4");

                return handler;
            })
            .AddTransientHttpErrorPolicy(builder => builder.WaitAndRetryAsync(new[]
            {
                // The AddTransientHttpErrorPolicy handles errors typical of Http calls:
                // Network failures (System.Net.Http.HttpRequestException)
                // HTTP 5XX status codes (server errors)
                // HTTP 408 status code (request timeout)
                TimeSpan.FromSeconds(1),
                TimeSpan.FromSeconds(5),
                TimeSpan.FromSeconds(10)
            }));

            // Register all the Windows of the applications.
            services.AddSingleton<WeatherAppWindow>();
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            var weatherWindow = host.Services.GetRequiredService<WeatherAppWindow>();
            weatherWindow.Show();

            base.OnStartup(e);
        }
    }
}