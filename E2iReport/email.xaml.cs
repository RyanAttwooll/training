﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace E2iReport
{
    /// <summary>
    /// Interaction logic for email.xaml
    /// </summary>
    public partial class email : Window
    {
        public email()
        {
            InitializeComponent();
        }

        private void AttachmentButton_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.Multiselect = true;
            dlg.Title = "Select Attachments";

            
            dlg.Filter = "All files (*.*)|*.*";

            if (dlg.ShowDialog() == true)
            {
                foreach (String file in dlg.FileNames)
                {
                    string fileName = System.IO.Path.GetFileName(file);
                    attachmentTxt.Text = file;
                }
            }
        }

        private void SendButton_Click(object sender, RoutedEventArgs e)
        {
            //MailMessage mail = new MailMessage();
            ////put your SMTP address and port here.
            //SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
            ////Put the email address
            //mail.From = new MailAddress("coolryan3436@gmail.com");
            //mail.To.Add("ryanattwooll@gmail.com");

            //mail.Subject = "File below";

            //System.Net.Mail.Attachment attachment = new System.Net.Mail.Attachment(attachmentTxt.Text);
            //mail.Attachments.Add(attachment);

            //SmtpServer.Credentials = new System.Net.NetworkCredential("coolryan3436@gmail.com", "coolryan");
            ////Set Smtp Server port
            //SmtpServer.Port = 25;
            //SmtpServer.EnableSsl = true;
            //SmtpServer.Timeout = 10000;

            //SmtpServer.Send(mail);
            //MessageBox.Show("The exception has been sent! :)");


            MailMessage mail = new MailMessage();
            //put your SMTP address and port here.
            SmtpClient SmtpServer = new SmtpClient("smtp.live.com");
            //Put the email address
            mail.From = new MailAddress("ryan.attwooll@bridmet.co.uk");
            mail.To.Add(tbSender.Text);

            mail.Subject = "File below";

            System.Net.Mail.Attachment attachment = new System.Net.Mail.Attachment(attachmentTxt.Text);
            mail.Attachments.Add(attachment);

            SmtpServer.Credentials = new System.Net.NetworkCredential("ryan.attwooll@bridmet.co.uk", "41ner_bmet");
            //Set Smtp Server port
            SmtpServer.Port = 25;
            SmtpServer.EnableSsl = true;
            SmtpServer.Timeout = 10000;

            SmtpServer.Send(mail);
            MessageBox.Show("The exception has been sent! :)");
        }
    }
}
