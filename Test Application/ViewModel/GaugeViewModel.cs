﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Test_Application
{
    public class GaugeViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(string info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

        public GaugeViewModel()
        {
            Angle = -180;
            Value = 0;
        }


        int _angle;
        public int Angle
        {
            get
            {
                return _angle;
            }

            private set
            {
                _angle = value;
                NotifyPropertyChanged("Angle");

            }
        }

        int _value;

        public int Value
        {
            get
            {
                return ((int)(_value / 1.8));
            }

            set
            {
                if (value >= 0 && value <= 180)
                {
                    _value = value;
                    Angle = value - 180;
                    NotifyPropertyChanged("Value");
                }
            }
        }

        public int ValueSlider
        {
            get
            {
                return (_value);
            }

            set
            {
                if (value >= 0 && value <= 180)
                {
                    _value = value;
                    Angle = value - 180;
                    NotifyPropertyChanged("Value");
                }
            }
        }
    }
}
