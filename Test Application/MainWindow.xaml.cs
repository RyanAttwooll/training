﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using Test_Application;
using WeatherApp;
using WeatherApp.CurrentWeatherApp;
using Jira;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    
    
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += Timer_Tick;
            timer.Start();
        }

        private void change_Click(object sender, RoutedEventArgs e)
        {
            Greeting.Content = "Good";
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            int Time = DateTime.Now.Hour;
            if (Time > 12 && Time < 18) 
            {
                Greeting.Content = "GoodAfternoon";
            }
            else if (Time > 18 && Time < 0)
            {
                Greeting.Content = "GoodEvening";
            }
            else
            {
                Greeting.Content = "GoodMorning";
            }
            
        }

        private void barcode_Click(object sender, RoutedEventArgs e)
        {
            BarcodeScanner objBarcodeScanner = new BarcodeScanner();
            this.Visibility = Visibility.Hidden;
            objBarcodeScanner.Show();
        }

        private void Gauge_Click(object sender, RoutedEventArgs e)
        {
            Gauge objGauge = new Gauge();
            this.Visibility = Visibility.Hidden;
            objGauge.Show();
        }

        private void JiraBtn_Click(object sender, RoutedEventArgs e)
        {
            Jira.MainWindow Jira = new Jira.MainWindow();
            Jira.Show();
        }

        private void ReportBtn_Click(object sender, RoutedEventArgs e)
        {
            E2iReport.MainWindow E2iReport = new E2iReport.MainWindow();
            E2iReport.Show();
        }

        private void scheduleBtn_Click(object sender, RoutedEventArgs e)
        {
            datepicker datepicker = new datepicker();
            this.Visibility = Visibility.Hidden;
            datepicker.Show();
        }
    }
}
