﻿using Syncfusion.UI.Xaml.Scheduler;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows;
using System.Windows.Controls;

namespace Test_Application
{
    /// <summary>
    /// Interaction logic for datepicker.xaml
    /// </summary>
    public partial class datepicker : Window
    {
        public datepicker()
        {
            this.InitializeComponent();


            ScheduleAppointmentCollection appointmentCollection = new ScheduleAppointmentCollection();
            SqlConnection con = new SqlConnection(@"Data Source=DESKTOP-SONPHPF\SQLEXPRESS;Initial Catalog=Holidays;Integrated Security=True");
            string sql = "Select FirstName, LastName, StartDate, EndDate from [Holidays].[dbo].[Holidays]";
            con.Open();
            SqlCommand cmd = new SqlCommand(sql, con);
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                ScheduleAppointment item = new ScheduleAppointment();
                item.StartTime = (DateTime)dr["StartDate"];           
                item.EndTime = (DateTime)dr["EndDate"];
                item.Subject = (string)dr["Firstname"]+ " " + dr["LastName"];
                appointmentCollection.Add(item);
                Schedule.ItemsSource = appointmentCollection;
            }

        }

    }
}
