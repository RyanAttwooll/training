﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Test_Application
{
    /// <summary>
    /// Interaction logic for UserControlGauge.xaml
    /// </summary>
    public partial class UserControlGauge : UserControl
    {
        public UserControlGauge()
        {
            InitializeComponent();
            this.DataContext = new GaugeViewModel();
        }
    }
}
